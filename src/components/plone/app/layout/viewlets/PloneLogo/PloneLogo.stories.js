import { withKnobs, text } from '@storybook/addon-knobs';
import PloneLogo from './PloneLogo.vue';

export default {
  title: 'Components|plone.app.layout.viewlets.PloneLogo',
  component: PloneLogo,
  decorators: [withKnobs],
  parameters: {
    componentSubtitle: 'Displays a linked image that represents the website',
  }
};

export const PloneLogoDefault = () => ({
  components: { PloneLogo },
  props: {
    logoTitle: {
      default: text('Logo Title', 'Home')
    },
    siteTitle: {
      default: text('Site Title', 'Plone Site')
    }
  },
  template: '<PloneLogo :logoTitle="logoTitle" :siteTitle="siteTitle"></PloneLogo>'
});

PloneLogoDefault.story = { name: 'Default' };
