import PloneHeader from './PloneHeader.vue';

export default {
  title: 'Components|plone.app.layout.viewlets.PloneHeader',
  component: PloneHeader,
  parameters: {
    componentSubtitle: 'The portal header',
  }
};

export const PloneHeaderDefault = () => ({
  components: { PloneHeader },
  template: '<PloneHeader></PloneHeader>'
});

PloneHeaderDefault.story = { name: 'Default' };
