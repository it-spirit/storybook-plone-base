import PloneSearchbox from './PloneSearchbox.vue';

export default {
  title: 'Components|plone.app.layout.viewlets.PloneSearchbox',
  component: PloneSearchbox,
  parameters: {
    componentSubtitle: 'The search box',
  }
};

export const PloneSearchboxDefault = () => ({
  components: { PloneSearchbox },
  template: '<PloneSearchbox></PloneSearchbox>'
});

PloneSearchboxDefault.story = { name: 'Default' };
