import { addParameters, addDecorator, configure } from '@storybook/vue';
import { withA11y } from '@storybook/addon-a11y';
import ploneTheme from './ploneTheme';

import { addons, makeDecorator } from '@storybook/addons';
import { EVENT_CODE_RECEIVED } from '@whitespace/storybook-addon-html/lib/shared';
import { mount } from '@vue/test-utils';
import '!style-loader!css-loader!sass-loader!../src/css/style.scss';
 
const withHTML = makeDecorator({
  name: 'withHTML',
  parameterName: 'html',
  skipIfNoParametersOrOptions: false,
  wrapper: (getStory, context, {options = {}}) => {
    const channel = addons.getChannel();
    const wrapper = mount(getStory());
    channel.emit(EVENT_CODE_RECEIVED, {html: wrapper.html(), options});
    return getStory();
  },
});

addDecorator(withA11y);
addDecorator(
  withHTML({
    prettier: {
      tabWidth: 2,
      useTabs: false,
      htmlWhitespaceSensitivity: 'ignore',
    },
  }),
);

addParameters({
  options: {
    selectedPanel: 'storybook/a11y/panel',
    theme: ploneTheme
  }
});
